import 'package:flutter/material.dart';
import 'package:flutter_travel_story/model/User.dart';
import 'package:hexcolor/hexcolor.dart';


class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              color: Colors.white,
            ),
            flex: 1,
          ),
          Expanded(
            child: Container(
              color: Colors.white,
              child: MenuSignInSignUp(),
            ),
            flex: 7,
          ),
        ],
      ),
    );
  }
}

class MenuSignInSignUp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MenuSignInSignUpSate();
  }
}

class _MenuSignInSignUpSate extends State<MenuSignInSignUp> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _rgEmailController = TextEditingController();
  TextEditingController _rgUserController = TextEditingController();
  TextEditingController _rgPasswordController = TextEditingController();
  bool _statusOfSingIn = true;
  List<User> _users;
  String _lEmail;
  String _lPassword;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            buttonSignIn(),
            buttonSignUp(),
          ],
        ),
        _statusOfSingIn ? layoutInfoSignIn() : layoutInfoSignUp()
      ],
    );
  }

  Widget buttonSignIn() {
    return GestureDetector(
      onTap: () {
        setState(() {
          _statusOfSingIn = true;
        });
      },
      child: Container(
        margin: EdgeInsets.only(right: 30),
        height: 30,
        width: 70,
        padding: EdgeInsets.all(5),
        child: Center(
          child: Text(
            "SIGN IN",
            style: TextStyle(
                fontStyle: FontStyle.normal,
                color: _statusOfSingIn ? Colors.white : HexColor("#707070")),
          ),
        ),
        decoration: BoxDecoration(
          color: _statusOfSingIn ? HexColor("#8A56AC") : Colors.white,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
      ),
    );
  }

  Widget buttonSignUp() {
    return GestureDetector(
      onTap: () {
        setState(() {
          _statusOfSingIn = false;
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: 30),
        height: 30,
        width: 70,
        padding: EdgeInsets.all(5),
        child: Center(
          child: Text(
            "SIGN UP",
            style: TextStyle(
                fontStyle: FontStyle.normal,
                color: _statusOfSingIn ? HexColor("#707070") : Colors.white),
          ),
        ),
        decoration: BoxDecoration(
          color: _statusOfSingIn ? Colors.white : HexColor("#8A56AC"),
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(50)),
        ),
      ),
    );
  }

  Widget layoutInfoSignIn() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
          child: TextField(
            controller: _emailController,
            decoration: InputDecoration(
              hintText: "Email",
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
          child: TextField(
            controller: _passwordController,
            obscureText: true,
            decoration: InputDecoration(
              hintText: "Password",
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20),
          child: GestureDetector(
            onTap: () {
              User user = User("thien", "thiennh", "123");
              _users = List<User>();
              _users.add(user);
              if(_users == null){
                _showMyDialog();
              }else{
                for(int i = 0; i <= _users.length - 1; i++){
                  if(_users[i].email == _emailController.text && _users[i].password == _passwordController.text){
                    print("ok");
                  }else
                    _showMyDialog();
                }
              }
            },
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width - 60,
              child: Center(
                child: Text(
                  "CONTINUE",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              decoration: BoxDecoration(
                  color: HexColor("#8A56AC"),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(52))),
            ),
          ),
        )
      ],
    );
  }

  Widget layoutInfoSignUp() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
          child: TextField(
            decoration: InputDecoration(
              hintText: "Name",
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
          child: TextField(
            decoration: InputDecoration(
              hintText: "Email",
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 10, 30, 0),
          child: TextField(
            decoration: InputDecoration(
              hintText: "Password",
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 20),
          child: GestureDetector(
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width - 60,
              child: Center(
                child: Text(
                  "CONTINUE",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              decoration: BoxDecoration(
                  color: HexColor("#8A56AC"),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(52))),
            ),
          ),
        )
      ],
    );
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Account is not exist'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Do you want to register for a new account?'),
              ],
            ),
          ),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                width: 100,
                height: 40,
                child: Center(
                  child: Text(
                    "CANCEL",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                decoration: BoxDecoration(
                    color: HexColor("#8A56AC"),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
            ),
            GestureDetector(
              onTap: () {
                _statusOfSingIn = false;
                Navigator.of(context).pop();
              },
              child: Container(
                width: 100,
                height: 40,
                child: Center(
                  child: Text(
                    "REGISTER",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
                decoration: BoxDecoration(
                    color: HexColor("#8A56AC"),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
            ),
          ],
        );
      },
    );
  }
}
