import 'package:flutter/material.dart';
import 'package:flutter_travel_story/ui/login/LoginScreen.dart';
import 'package:hexcolor/hexcolor.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: HexColor("#9599B3"),
      body: Stack(
        overflow: Overflow.visible,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      image: DecorationImage(
                          image: AssetImage("images/backround.jpg"),
                          fit: BoxFit.fitWidth)),
                ),
                flex: 8,
              ),
              Expanded(
                child: Container(),
                flex: 2,
              ),
            ],
          ),
          Positioned(
            child: Container(
              height: 150,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Welcome to Travel Story",
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            color: Colors.white),
                      ),
                    ],
                  )
                ],
              ),
              decoration: BoxDecoration(
                color: HexColor("#5F4E6A"),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(80),
                ),
              ),
            ),
            bottom: 60,
            left: 20,
          ),
          Positioned(
            child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              },
              child: Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "GET STARTED",
                    style: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                        fontStyle: FontStyle.italic),
                  ),
                ),
                decoration: BoxDecoration(
                  color: HexColor("#8A56AC"),
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(50),
                      bottomLeft: Radius.circular(50)),
                ),
              ),
            ),
            bottom: 40,
            left: MediaQuery.of(context).size.width - 150,
          ),
        ],
      ),
    );
  }
}
