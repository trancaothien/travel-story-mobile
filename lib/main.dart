import 'package:flutter/material.dart';
import 'package:flutter_travel_story/ui/splash/SplashScreen.dart';

void main(){
  runApp(
    MaterialApp(
      home: SplashScreen(),
    )
  );
}